<?php

namespace App\Validator;

use phpDocumentor\Reflection\Types\Parent_;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class BanWord extends Constraint
{
    public function __construct(
        public $message = 'This contains a banned word "{{ banWord }}".',
        public array $BanWords = ['spam', 'viagra'],
        array $groups = null,
        mixed $payload = null)
    {

        parent::__construct(null, $groups, $payload);
    }

}
