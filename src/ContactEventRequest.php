<?php

namespace App;

use App\DTO\ContactDTO;

readonly class ContactEventRequest
{

    public function __construct(public ContactDTO $data)
    {

    }

}