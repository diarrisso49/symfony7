<?php

namespace App\Controller\Admin;

use AllowDynamicProperties;
use App\Entity\Categories;
use App\Form\CategoryType;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AllowDynamicProperties] #[Route('/admin/categories', name: 'admin.categories.')]
#[IsGranted('ROLE_ADMIN')]
class CategoriesController extends AbstractController
{

    public function __construct( private readonly EntityManagerInterface $entityManager)
    {
    }


    #[Route('/', name: 'index')]
    public function index( CategoriesRepository $categoriesRepository): Response
    {
        $categories = $categoriesRepository->findAllWithCount();


        return $this->render('admin/categories/index.html.twig', [
            'categories' => $categoriesRepository->findAllWithCount(),
        ]);
    }

    #[Route('/create', name: 'create', methods: ['GET','POST'])]
    public function create( Request $request): \Symfony\Component\HttpFoundation\RedirectResponse|Response
    {
        $categories = new  Categories();
        /**
         *
         */
        $form = $this->createForm(CategoryType::class, $categories);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($categories);
            $this->entityManager->flush();
            $this->addFlash('success', 'categories has be created');

             return $this->redirectToRoute('admin.categories.index');
        }
        return $this->render('admin/categories/create.html.twig', [
            'form' => $form
        ]);

    }

    #[Route('/{id}', name: 'edit')]
    public function edit(Categories $categories, Request $request): \Symfony\Component\HttpFoundation\RedirectResponse|Response
    {
        $form = $this->createForm(CategoryType::class,$categories );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            $this->addFlash('success', 'the category has be edited');
            return $this->redirectToRoute('admin.categories.index');
        }
        return $this->render('admin/categories/edit.html.twig', [
            'form' => $form,
            'categories' => $categories
        ]);

    }
    #[Route('/delete/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE'])]
    public function removeCategory(Categories $categories): RedirectResponse
    {
        $this->entityManager->remove($categories);
        $this->entityManager->flush();
        $this->addFlash('success', 'the categories has be deleted ');
        return $this->redirectToRoute('admin.categories.index');
    }


}
