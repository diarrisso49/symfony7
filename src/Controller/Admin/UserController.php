<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use function MongoDB\BSON\toJSON;

class UserController extends AbstractController
{
    #[Route('/api/me')]
    #[IsGranted("ROLE_USER")]
    public function login(): Response
    {
        return  $this->json($this->getUser());
    }
}
