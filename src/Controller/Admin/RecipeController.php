<?php

namespace App\Controller\Admin;

use App\Entity\Recipe;
use App\Entity\User;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use App\Security\Voter\RecipeVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

#[Route('/admin/recipes', name: 'admin.recipe.')]
class RecipeController extends AbstractController
{
    public function __construct(private readonly Security $security)
    {
    }

    #[Route('/', name: 'index' )]
    public function index( RecipeRepository $recipeRepository, Request $request, Security $security ): Response
    {
        $page = $request->query->getInt('page', 1);
        $userId = $security->getUser()->getId();
        $canListAll = $security->isGranted(RecipeVoter::LIST_All);
        $recipes = $recipeRepository->paginateRecipe($page, $canListAll ? null : $userId);
        return $this->render('admin/recipe/index.html.twig', [
            'recipes' => $recipes
        ]);
    }
   /* #[Route('/recipe/{slug}-{id}', name: 'recipe.show', requirements: ['slug' => '[a-z0-9-]+', 'id' => '\d+'] )]
    public function show( Request $request, string $slug, int $id, RecipeRepository $recipeRepository): Response
    {

        $recipe = $recipeRepository->find($id);
        if ($recipe->getSlug() !== $slug) {
            return $this->redirectToRoute('recipe.show', [
                'id' => $recipe->getId(),
                'slug' => $recipe->getSlug(),
            ], 301);
        }
        return $this->render('recipe/show.html.twig', [
           'recipe' => $recipe,
        ]);

    }*/

    #[Route('/create', name: 'create')]
    #[IsGranted(RecipeVoter::CREATE)]
    public  function create( Request $request, EntityManagerInterface $em ): Response
    {
        $recipe = new Recipe();
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->security->getUser();
            if ($user instanceof User) {
                $recipe->setUser($user);
            }

            $em->persist($recipe);
            $em->flush();
            $this->addFlash('success', 'Recipe has be created');

            return $this->redirectToRoute('admin.recipe.index');
        }

        return $this->render('admin//recipe/create.html.twig', [
           'form' => $form
        ]);
    }

    #[Route('/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS] , methods: ['POST', 'GET'])]
    #[IsGranted(RecipeVoter::EDIT, subject: 'recipe')]
    public function edit( Recipe $recipe, Request $request, EntityManagerInterface $em, UploaderHelper $helper ): Response
    {
        //dd($helper->asset($recipe, 'thumbnailFile'));
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        if ($form->isSubmitted() &&  $form->isValid()) {
           /* /** @var  UploadedFile $file */
            /*$file = $form->get('thumbnailFile')->getData();
            $filename = $recipe->getId() . '.' . $file->getClientOriginalExtension();
            $file->move($this->getParameter('kernel.project_dir') . '/public/recipes/images', $filename);
            $recipe->setThumbnail($filename);*/
            $em->flush();
            $this->addFlash('success', 'the recipe was well edit');

            return  $this->redirectToRoute('admin.recipe.index');
        }

        return  $this->render('admin/recipe/edit.html.twig', [
           'recipe' => $recipe,
           'form' => $form
        ]);
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE'])]
    public function removeRecipe(Recipe $recipe, EntityManagerInterface $em): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $em->remove($recipe);
        $em->flush();
        $this->addFlash('success', 'the recipe was well deleted ');
        return $this->redirectToRoute('admin.recipe.index');
    }
}


