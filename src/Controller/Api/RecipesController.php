<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\DTO\PaginationDTO;
use App\Entity\Recipe;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class RecipesController extends AbstractController
{
    #[Route('/api/recipes', methods:  ['GET'])]
    public function index(
        RecipeRepository $recipeRepository,
        #[MapQueryString]
        ?PaginationDTO $paginationDTO = null
    ): \Symfony\Component\HttpFoundation\JsonResponse
    {
        //$recipes = $recipeRepository->paginateRecipe($request->query->getInt('page', 1));
        $recipes = $recipeRepository->paginateRecipe($paginationDTO?->page);
        //dd($serializer->serialize($recipes, 'yaml'));
        return  $this->json($recipes, 200, [], [
            'groups' => ['recipes.index']
        ]);
    }

    #[Route('/api/recipes/{id}', requirements: ['id' => Requirement::DIGITS], methods: ['GET'])]
    public function show(Recipe $recipe): \Symfony\Component\HttpFoundation\JsonResponse
    {
        return  $this->json($recipe, 200, [], [
            'groups' => ['recipes.index', 'recipes.show']
        ]);
    }

    #[Route('/api/recipes', methods: ['POST'])]
    public function create(
        EntityManagerInterface $em,
        #[MapRequestPayload(
            serializationContext: [
                'groups' => ['recipes.create']
            ]
        )]
        Recipe $recipe)
    {
        $recipe->setCreatedAt(new \DateTimeImmutable());
        $recipe->setUpdatedAt(new \DateTimeImmutable());
        $em->persist($recipe);
        $em->flush();

        return $this->json($recipe, 200, [], [
            'groups' => ['recipes.index', 'recipes.show']
        ]);
    }
}
