<?php

declare(strict_types=1);

namespace App\Controller;

use App\ContactEventRequest;
use App\DTO\ContactDTO;
use App\Entity\Recipe;
use App\Form\ContactType;
use App\Form\RecipeType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;

class ContactController extends AbstractController
{
    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/contact')]
    public function contact(Request $request , MailerInterface $mailer, EventDispatcherInterface $dispatcher): Response
    {
        $data = new ContactDTO();
        $form = $this->createForm(ContactType::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $dispatcher->dispatch(new ContactEventRequest($data));
                    $this->addFlash('success', 'email a ete bien envoyer');
                    return $this->redirectToRoute('home');
            } catch (\Exception $e) {
                $this->addFlash('danger', 'email can not be send');
                return $this->redirectToRoute('app_contact_contact');
            }
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    private function sendEmail(MailerInterface $mailer, $data): Response
    {
        $email = (new Email())
            ->from($data['email'])
            ->to('you@example.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text($data['name'])
            ->html('<p>' . $data['message'] . '</p>');

        $mailer->send($email);

    }
}