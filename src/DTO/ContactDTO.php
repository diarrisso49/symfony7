<?php

namespace App\DTO;


use Symfony\Component\Validator\Constraints as Assert;

class ContactDTO
{
    #[Assert\NotBlank()]
    public $name;
    #[Assert\NotBlank()]
    public $email;
    #[Assert\NotBlank()]
    public $message;
    public $service;

}