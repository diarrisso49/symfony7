<?php

namespace App\EventSubscriber;

use App\ContactEventRequest;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class MailingSubscriber implements EventSubscriberInterface
{
    public function __construct( private readonly MailerInterface $mailer)
    {

    }

    /**
     * @throws TransportExceptionInterface
     */
    public function onContactRequestEvent(  ContactEventRequest $event): void
    {
        $data = $event->data;
        $mail = (new TemplatedEmail())
            ->to($data->service)
            ->subject('Time for Symfony Mailer!')
            ->htmlTemplate('/mails/contact.html.twig')
            ->context(['data' => $data]);
        $this->mailer->send($mail);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function onLogin(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof User) {
            return;
        }

        $mail = (new Email())
            ->to($user->getEmail())
            ->from('support@demo.fr')
            ->subject('Login')
            ->text('you are login');
        $this->mailer->send($mail);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ContactEventRequest::class => 'onContactRequestEvent',
            InteractiveLoginEvent::class => 'onLogin'
        ];
    }
}
