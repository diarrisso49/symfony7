<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public  CONST ADMIN = "ADMIN_USER";
    public function __construct( private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }
    public function load(ObjectManager $manager): void
    {
         $user = new User();
         $user->setRoles(["ROLE_ADMIN"])
             ->setUsername("Pelico")
             ->setVerified(true)
             ->setPassword($this->userPasswordHasher->hashPassword($user, 'admin'))
             ->setEmail("app@gmail.com")
             ->setApiToken('admin_token');
         $this->addReference(self::ADMIN, $user);
        $manager->persist($user);

        for ($i = 1; $i <= 10; $i++) {
            $user = (new User())
                 ->setRoles([])
                ->setUsername("user{$i}")
                ->setVerified(true)
                ->setPassword($this->userPasswordHasher->hashPassword($user, '0000'))
                ->setEmail("user{$i}@gmail.com")
                ->setApiToken("user_api{$i}");
            $this->addReference('USER' . $i, $user);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
