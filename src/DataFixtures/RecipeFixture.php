<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Ingredient;
use App\Entity\Quantity;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use FakerRestaurant\Provider\en_US\Restaurant;
use Symfony\Component\String\Slugger\SluggerInterface;

class RecipeFixture extends Fixture implements DependentFixtureInterface
{

    public function __construct( private  readonly SluggerInterface $slugger)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker =  Factory::create('de_DE');
        $faker->addProvider(new Restaurant($faker));


        $ingredients = array_map( fn(string $name) => (new Ingredient())
              ->setName($name)
              ->setSlug(strtolower($this->slugger->slug($name))), [
            'Potato',
            'Tomato',
            'Onion',
            'Garlic',
            'Carrot',
            'Pepper',
            'Cucumber',
            'Lettuce',
            'Mushroom',
            'Spinach'

        ]);


        $units = [
            'kg',
            'ml' ,
            'kg',
            'L',
            'cl',
            'dl',
            'g'
        ];
        foreach ($ingredients as $ingredient) {
            $manager->persist($ingredient);
        }

        $categories = ['Roast Dinner', 'Shepherd', 'Cream Tea ', 'Ried Chicken' ];

        foreach ($categories as $categori) {

            $category = (new Categories())
                ->setName($categori)
                ->setSlug($this->slugger->slug($categori))
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()));
              $manager->persist($category);
              $this->addReference($categori, $category);
            $manager->flush();
        }

        for ($i = 1; $i <= 10; $i++) {
            $title = $faker->foodName();
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $title)));
            $recipe = ( new Recipe())
                ->setTitle($title)
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setContent($faker->paragraph(10, true ))
                ->setSlug($this->slugger->slug($title))
                ->setCategory($this->getReference($faker->randomElement($categories)))
                ->setDuration($faker->numberBetween(2, 60))
                ->setUser($this->getReference('USER' . $faker->numberBetween(1,10 )));

            foreach ($faker->randomElements($ingredients, $faker->numberBetween(2,5)) as $ingredient) {

                $recipe->addQuantity((new Quantity())
                   ->setQuantity($faker->numberBetween(1,250))
                    ->setUnit($faker->randomElement($units))
                    ->setIngredient($ingredient)
                );

            }
            $manager->persist($recipe);

        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
