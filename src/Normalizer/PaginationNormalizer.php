<?php

namespace App\Normalizer;

use App\Entity\Recipe;
use http\Exception\RuntimeException;
use Knp\Component\Pager\Pagination\PaginationInterface;
use phpDocumentor\Reflection\DocBlock\Serializer;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

readonly class PaginationNormalizer implements  NormalizerInterface
{

    public function __construct(
        #[Autowire(service: 'serializer.normalizer.object')]
        private NormalizerInterface $normalizer
    )
    {
    }

    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        if (!($object instanceof PaginationInterface)) {
            throw new \RuntimeException();
        }

        return [
            'items' => array_map( fn (Recipe $recipe) => $this->normalizer->normalize($recipe, $format, $context), $object->getItems()),
            'total' => $object->getTotalItemCount(),
            'page' => $object->getCurrentPageNumber(),
            'lastPage' => ceil($object->getTotalItemCount() / $object->getCurrentPageNumber())
        ];
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return  $data instanceof PaginationInterface  ;
    }

    public function getSupportedTypes(?string $format): array
    {
        return  [
            PaginationInterface::class => true
        ];
    }
}