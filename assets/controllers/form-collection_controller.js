import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    connect() {
       this.index = this.element.ChildElementCount
       const btn = document.createElement('button');
       btn.setAttribute('type', 'button');
       btn.setAttribute('class', 'btn btn-secondary');
       btn.innerHTML = 'Add new';
       btn.addEventListener('click', this.addNew)
       this.element.appendChild(btn)
       this.element.childNodes.forEach(this.addDeleteButton)

    }

    /**
     * Add new form collection
     * @param {MouseEvent} e
     */
    addNew = (e)  => {
        e.preventDefault()
        const element = document.createRange().createContextualFragment(
            this.element.dataset['prototype'].replaceAll('__name__', this.index)
        ).firstElementChild
        this.addDeleteButton(element)
        this.index++
        e.currentTarget.insertAdjacentElement('beforebegin', element)
    }

    /**
     * Remove form collection
     * @param {HTMLElement} item
     */
    addDeleteButton = (item) => {
        const btn = document.createElement('button');
        btn.setAttribute('type', 'button');
        btn.setAttribute('class', 'btn btn-secondary ');
        btn.innerHTML = 'Remove';
        item.appendChild(btn);
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            item.remove()
        })
    }
}
